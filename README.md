# EEE088F Group 29

A HAT design to be added to the STM32 microcontroller.

## Ambient Light Sensor Description



## Contents
This repo contains all files needed to understand the functionality of the HAT as well as have the HAT built. A description of the folder contents can be found below:

* [CAD](https://gitlab.com/morrisafrikao/ambient-light-sensor/-/tree/main/CAD) - Models and designs associated with the project

* [DOC](https://gitlab.com/morrisafrikao/ambient-light-sensor/-/tree/main/DOC) - Documentation which may include schematics, datasheets and manuals

* [FRM](https://gitlab.com/morrisafrikao/ambient-light-sensor/-/tree/main/FRM) - Source code for the HAT's software interface. 

* [PCB](https://gitlab.com/morrisafrikao/ambient-light-sensor/-/tree/main/PCB) - Contains PCB schematic diagrams 

* [PRD](https://gitlab.com/morrisafrikao/ambient-light-sensor/-/tree/main/PRD) - Documents relating to cost of HAT and production of HAT

* [SIM](https://gitlab.com/morrisafrikao/ambient-light-sensor/-/tree/main/SIM) - Contains documents neccessary for the fabrication of the HAT


## Support
Any questions concerning the project can be addressed to the project developers at NKMMOR001@myuct.ac.za; vjykes001@myuct.ac.za 


## Authors and acknowledgment
Project designers are Morris Nkomo and Keshav Vijayanath.


## Project status
Project is currently under development.
